package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchingdataApache {
	public static void main(String[] args) throws IOException {
		File f = new File("/home/global/Documents/Test_data/Testdata2.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet Sheet = book.getSheetAt(0);

		int rows = Sheet.getPhysicalNumberOfRows();
		for (int i = 0; i < rows; i++) {
			int colum = Sheet.getRow(i).getLastCellNum();
			for (int j = 0; j < colum; j++) {
				String cellvalue = Sheet.getRow(i).getCell(j).getStringCellValue();
				System.out.println(cellvalue);
			}
		}

	}

}
