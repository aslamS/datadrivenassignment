package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


public class Registerdatajxl {
	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		File f = new File("/home/global/Documents/Test_data/Testdata1.xls");
		FileInputStream fis = new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet s= book.getSheet("Registerdata");
		int rows= s.getRows();
		int column = s.getColumns();
		for(int i=1;i<rows;i++) {
			String firstname =s.getCell(0, i).getContents();
			String lastname = s.getCell(1, i).getContents();
			String email =s.getCell(2,i).getContents();
			String password = s.getCell(3,i).getContents();
			String confirm =s.getCell(4, i).getContents();
			driver.findElement(By.linkText("Register")).click();
			driver.findElement(By.id("gender-female")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(confirm);
			driver.findElement(By.id("register-button")).click();
			WebElement txt =driver.findElement(By.xpath("//li[contains(text(),'The specified email already exists')]"));
			txt.click();
			String t= txt.getText();
			System.out.println(t);
			driver.quit();
			
		}

	}
}
